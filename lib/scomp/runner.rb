#
# Runner for scomp (ie main program)
#

require File.expand_path('../logger', __FILE__)
require File.expand_path('../config', __FILE__)
require File.expand_path('../options', __FILE__)
require File.expand_path('../filemanager', __FILE__)

module SComp
  class Runner

    def initialize(argv)
      # Parse the comand line
      @options = Options.new(argv)
      exit(1) unless @options.valid
      # General logger for scomp
      @logger = MyLogger.new(STDOUT)
      @logger.level = @options.debug ? Logger::DEBUG : Logger::INFO
      # The SSH library is a bit too verbose, so we use a different logger
      @logger_ssh = MyLogger.new(STDOUT)
      @logger_ssh.level = @options.debug ? Logger::INFO : Logger::WARN
      @logger_ssh.prefix = "> "
    end

    def run
      begin
        log { @config = Config.new(@options, @logger) }
        log { @file_manager = FileManager.new(@config, @logger, @logger_ssh) }
        log { @file_manager.check_remote_directories }
        # Part 1 : getting a copy of the files
        log { @file_manager.copy_local_ticket_files_to_tmp }
        log { @file_manager.copy_remote_ticket_files_to_tmp }
        # Part 2 : The diff/comparison
        log { @file_manager.checksum_files_before }
        log { @file_manager.compare_directories }
        log { @file_manager.checksum_files_after }
        log { @file_manager.check_files_syntax }
        # Part 3 : Deploying
        log { @file_manager.ask_for_confirmation }
        log { @file_manager.deliver_payload }
      rescue Exception => e
        @logger.error(e)
        @logger.debug(e.backtrace.join("\n"))
        @logger.info("")
      ensure
        @file_manager.cleanup if @file_manager
      end
    end

    private

    # Runs the block and output a new line
    def log
      yield ; @logger.info("")
    end

  end
end

# vim: set et ts=2 sw=2 sts=2:
