#
# Personalized logger for SComp
#

require 'logger'

# This class provides a logger, already configured with good defaults (at least
# defaults I like). It also provides a few convenience methods for dealing with
# colors.
class MyLogger < Logger
  DEFAULT_LEVEL = INFO
  attr_accessor :prefix

  def initialize(logdev, shift_age = 0, shift_size = 1048576)
    super(logdev, shift_age, shift_size)
    self.formatter = proc do |severity, datetime, progname, msg|
      "[#{datetime}] #{@prefix}#{msg}\n"
    end
    self.level = DEFAULT_LEVEL
  end

  # Separate content from presentation
  def error(msg); super(red(msg)); end
  def header(text); info(brown(text)); end
  def command(text); info(grey(text)); end

  # Technically, those should be class methods, not instance methods. And they
  # probably should be separated in another class. They don't have to belong in
  # the logger. However, for simplicity's sake, I'll keep them here. I hope I
  # won't burn in hell for it.
  def red(text) ; colorize(text, "\e[31m") ; end
  def grey(text) ; colorize(text, "\e[37m") ; end
  def blue(text) ; colorize(text, "\e[36m") ; end
  def green(text) ; colorize(text, "\e[32m") ; end
  def brown(text) ; colorize(text, "\e[33m") ; end

  private

  def colorize(text, color)
    # It would probably be good to have some way of knowing whether the output
    # device is capable of handling color codes, and make a decision based on
    # that. However, the program is an interactive shell program that requires
    # human interaction. It will thus run in a shell 99.9999% of the time. So
    # let's not over do it.
    "#{color}#{text}\e[0m"
  end

end

# vim: set et ts=2 sw=2 sts=2:
