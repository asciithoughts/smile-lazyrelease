#
# Configuration for Scomp
#

require 'yaml'

module SComp

  class ConfigException < Exception
  end

  class Config

    DEFAULT_PORT = 22 # SSH

    attr_reader :host, :port, :user, :password, :remote_dir
    attr_reader :remote_backup, :remote_backup_dir
    attr_reader :local_backup, :local_backup_dir
    attr_reader :ticket_file

    def initialize(options, logger)
      @logger = logger
      read_config(options.config)
      save_server(options.server)
      save_ticket(options.ticket)
    end

    def remote_backup_filename
      time = Time.now.strftime("%Y-%m-%d__%Hh%Mm%Ss")
      "#{@server}__#{time}__backup__#{@ticket_name}"
    end

    def remote_deploy_filename
      time = Time.now.strftime("%Y-%m-%d__%Hh%Mm%Ss")
      "#{@server}__#{time}__payload__#{@ticket_name}"
    end

    def ticket_file_include
      "#{@ticket_name}.include.txt"
    end

    def ticket_file_exclude
      "#{@ticket_name}.exclude.txt"
    end

    private

    def read_config(config)
      unless File.readable?(config)
        raise ConfigException, "Configuration file '#{config}' is not readable"
      end
      @logger.header('Loading configuration')
      File.open(config) do |file|
        @yaml = YAML::load(file)
      end
    end

    def save_server(server)
      unless @yaml && @yaml[server]
        raise ConfigException, "Could not find configuration for server '#{server}'"
      end
      @server = server
      @host = @yaml[server]["host"]
      @port = @yaml[server]["port"]
      @user = @yaml[server]["user"]
      @password = @yaml[server]["password"]
      @remote_dir = @yaml[server]["remote_dir"]
      @local_backup = @yaml[server]["local_backup"]
      @local_backup_dir = @yaml[server]["local_backup_dir"]
      @remote_backup = @yaml[server]["remote_backup"]
      @remote_backup_dir = @yaml[server]["remote_backup_dir"]
      validate_server
    end

    def validate_server
      justify = 20

      # HOST
      if @host and not @host.empty?
        @logger.info("- Hostname".ljust(justify) + " : #{@host}")
      else
        raise ConfigException, "No hostname provided!"
      end

      # PORT
      if @port.instance_of? String and @port.to_i > 0
        @port = @port.to_i
      elsif @port.instance_of? Fixnum and @port > 0
        # do nothing
      else
        @port = DEFAULT_PORT
      end
      @logger.info("- Port".ljust(justify) + " : #{@port}")

      # USERNAME
      if @user and not @user.empty?
        @logger.info("- Username".ljust(justify) + " : #{@user}")
      else
        raise ConfigException, "No username provided!"
      end

      # PASSWORD
      if @password and not @password.empty?
        @logger.info("- Password".ljust(justify) + " : #{"*" * @password.length}")
      else
        @password = false
      end

      # REMOTE DIRECTORY
      if @remote_dir and not @remote_dir.empty?
        @logger.info("- Remote dir".ljust(justify) + " : #{@remote_dir}")
      else
        raise ConfigException, "No remote directory provided!"
      end

      # BACKUP
      @local_backup = (@local_backup === true || @local_backup === "yes" || @local_backup === 1)
      @remote_backup = (@remote_backup === true || @remote_backup === "yes" || @remote_backup === 1)

      # LOCAL BACKUP_DIR
      if @local_backup
        if @local_backup_dir and not @local_backup_dir.empty?
          if File.directory?(@local_backup_dir)
            @logger.info("- Local backup dir".ljust(justify) + " : #{@local_backup_dir}")
          else
            raise ConfigException, "Local backup directory '#{@local_backup_dir}' does not exist or is not a directory"
          end
        else
          raise ConfigException, "Local backup selected but no local backup directory provided!"
        end
      end

      # REMOTE BACKUP DIR
      if @remote_backup
        if @remote_backup_dir and not @remote_backup_dir.empty?
          @logger.info("- Remote backup dir".ljust(justify) + " : #{@remote_backup_dir}")
        else
          raise ConfigException, "Remote backup selected but no remote backup directory provided!"
        end
      end
    end

    def save_ticket(ticket)
      unless ticket =~ /[0-9][\w-]*\.txt/
        raise ConfigException, "Ticket file '#{ticket}' is invalid. Its name should be 12345-description.txt"
      end
      unless File.readable?(ticket)
        raise ConfigException, "Ticket file '#{ticket}' is not readable"
      end
      @ticket_file = ticket
      @ticket_name = ticket.sub(/\.txt$/, '')
      validate_ticket_file
    end

    # All the lines in the ticket file should be existing paths in the current
    # directory. This file will be fed to 'tar' later, and tar will complain if
    # there are paths it can't find.
    def validate_ticket_file
      lines = 0
      errors = false
      included_files = []
      excluded_files = []

      File.open(@ticket_file) do |file|
        file.each_line do |line|
          lines += 1
          path = line.chomp
          if path =~ /^- /
            path.gsub! /^- /, ''
            excluded_files << path
          else
            included_files << path
            unless File.exist?(path)
              @logger.warn("> #{path} doesn't exist")
              errors = true
            end
          end
        end
      end

      if lines == 0 || errors
        raise ConfigException, "Ticket file '#{@ticket_file}' is invalid"
      end

      # Create the include file
      included_files << '.' if included_files.empty?
      File.open(ticket_file_include, 'w') do |file|
        file.write included_files.join "\n"
      end

      # Create the exclude file
      File.open(ticket_file_exclude, 'w') do |file|
        file.write excluded_files.join "\n"
      end

    end

  end
end

# vim: set et ts=2 sw=2 sts=2:
