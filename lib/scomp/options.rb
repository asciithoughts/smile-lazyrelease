#
# Command line option parsing for scomp
#

require 'optparse'

module SComp
  class Options
    CONFIG = "delivery.yml"
    attr_reader :valid
    attr_reader :debug
    attr_reader :config
    attr_reader :server
    attr_reader :ticket

    def initialize(argv, out=STDOUT, err=STDERR)
      @config = CONFIG
      @valid = false
      @out = out
      @err = err
      parse(argv)
    end

    private

    # Parses the command line for options and required arguments.
    def parse(argv)
      OptionParser.new do |opts|
        # Defining options
        opts.banner = "Usage: scomp [-c config] server ticket.txt"
        opts.on("-c config", "Configuration file (default: #{CONFIG})") { |c| @config = c }
        opts.on("-d", "--debug", "Show stacktraces") { @debug = true }
        opts.on_tail("-h", "--help", "Show this message") {}

        # Parsing command line
        begin
          opts.parse!(argv)
          if argv.size == 2
            @valid = true
            @server = argv[0]
            @ticket = argv[1]
          end
        rescue OptionParser::ParseError => e
          @err.puts e, "\n"
        end

        # Showing usage if something went wrong
        @out.puts opts unless @valid
      end
    end

  end
end

# vim: set et ts=2 sw=2 sts=2:
