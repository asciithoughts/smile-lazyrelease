#
# File Manager.
#
# This class manages the SSH and SFTP connection, as well as copying files
# around, launching commands, making tars, etc.. It is basically the core of
# scomp: where the real work happens.
#
# Since this file is pretty long, I'll use visual comment to separate different
# sections of code. All methods will thus be considered private except those
# specifically declared public.
#
# A number of choices have been made regarding copying files, in particular
# using 'tar' instead of scp, rsync, or anything else. Please refer to the
# README or FAQ for a better understanding of those choices.
#

require 'rubygems'
require 'net/ssh'
require 'net/sftp'
require 'fileutils'
require 'digest/md5'
require File.expand_path('../checker', __FILE__)

module SComp
  class FileManager

    VISUAL_DIFF = ENV['MERGETOOL'] || "meld"

    def initialize(config, logger, logger_ssh)
      @time = Time.new
      @config = config
      @logger = logger
      @logger_ssh = logger_ssh

      @tmp_local = "/tmp/scomp-local-#{"%.6f" % @time.to_f}"
      @tmp_remote = "/tmp/scomp-remote-#{"%.6f" % @time.to_f}"
      @tmp_local_entries = [] # we store everything created to clean it up at the end
      @tmp_remote_entries = [] # we store everything created to clean it up at the end

      @tmp_local_entries << config.ticket_file_include
      @tmp_local_entries << config.ticket_file_exclude

      @files_md5 = {}
      @files_added = []
      @files_deleted = []
      @files_modified = []

      initialize_ssh
    end

    private

    ###########################################################################
    # Cleanup
    ###########################################################################

    def cleanup
      @logger.header("Cleaning up")
      confirm
      cleanup_directories
      @logger.info("")
      cleanup_ssh
    end

    def cleanup_directories
      cleanup_local_directories
      @logger.info("")
      cleanup_remote_directories
    end

    def cleanup_local_directories
      @logger.header("Deleting local temporary files & directories")
      @tmp_local_entries.each do |path|
        if File.exists?(path)
          @logger.info("> Deleting local  : #{path}")
          begin
            FileUtils.remove_entry_secure(path)
          end
        end
      end
    end

    def cleanup_remote_directories
      @logger.header("Deleting remote temporary files & directories")
      @tmp_remote_entries.each do |path|
        if remote_exists?(path)
          @logger.info("> Deleting remote : #{path}")
          begin
            @sftp.remove!(path)
          end
        end
      end
    end

    ###########################################################################
    # SSH / SFTP
    ###########################################################################

    def initialize_ssh
      @logger.header("Opening SSH connection")
      options = {}
      options[:logger] = @logger_ssh
      options[:port] = @config.port if @config.port
      options[:password] = @config.password if @config.password
      @ssh = Net::SSH.start(@config.host, @config.user, options)
      @sftp = @ssh.sftp(true)
      @logger.info("> OK")
    end

    def cleanup_ssh
      @logger.header("Closing SSH connection")
      begin
        @sftp.close_channel if @sftp
        @ssh.close if @ssh
        @logger.info("> OK")
      rescue
        @logger.error("> KO")
      end
    end

    ###########################################################################
    # Checking configuration
    ###########################################################################

    def check_remote_directories
      @logger.header("Checking remote directories")
      unless remote_exists?(@config.remote_dir)
        raise ConfigException, "The remote directory '#{@config.remote_dir}' could not be found!"
      end
      unless remote_exists?(@config.remote_backup_dir)
        raise ConfigException, "The remote backup directory '#{@config.remote_backup_dir}' could not be found!"
      end

      @logger.info("> OK")
    end

    ###########################################################################
    # Part 1 : getting a copy of the files
    ###########################################################################

    def copy_local_ticket_files_to_tmp
      @logger.header("Copying local ticket files to tmp")

      # We could do the copy manually and recursively, but that would be long
      # and cumbersome. Instead we create a tar file, and then uncompress it in
      # /tmp. This method has the advantage of being fast and usable both
      # locally and remotely.

      @tmp_local_entries << tmpdir = @tmp_local
      @tmp_local_entries<< tmptar = @tmp_local + ".local.tgz"

      myexec("mkdir", tmpdir)
      myexec("tar", "-czf", tmptar, "-T", @config.ticket_file_include, "-X", @config.ticket_file_exclude)
      myexec("tar", "-xzf", tmptar, {:chdir => tmpdir})

      @logger.info("> OK")
    end

    def copy_remote_ticket_files_to_tmp
      @logger.header("Copying remote ticket files to tmp")

      @tmp_local_entries << tmpdir = @tmp_remote
      @tmp_local_entries << tmptar = @tmp_remote + ".local.tgz"
      @tmp_remote_entries << tmptar_remote = @tmp_remote + ".remote.tgz"

      # Remote include file
      @tmp_local_entries << ticket = create_remote_ticket_file
      @tmp_remote_entries << ticket_remote = ticket + ".remote"
      @sftp.upload!(ticket, ticket_remote)

      # Remote exclude file
      ticket_exclude = @config.ticket_file_exclude
      ticket_remote_exclude = @config.remote_dir + "/" + ticket_exclude
      @tmp_remote_entries << ticket_remote_exclude
      @sftp.upload!(ticket_exclude, ticket_remote_exclude)

      sshexec("cd \"#{@config.remote_dir}\" && tar -czf \"#{tmptar_remote}\" -T \"#{ticket_remote}\" -X \"#{ticket_remote_exclude}\"")
      @sftp.download!(tmptar_remote, tmptar)
      myexec("mkdir", tmpdir)
      myexec("tar", "-xzf", tmptar, {:chdir => tmpdir})

      @logger.info("> OK")
    end

    def create_remote_ticket_file
      @logger.info("> Checking remote paths")
      # Since we are trying to deploy a list of local files to the remote
      # server, it is possible that some of those files/directories do not exist
      # yet on the server. If we simply use the tar method with the original
      # ticket file list, tar will produce error. So instead, we create a new
      # ticket file, on the server directly, containing only those path that
      # exists.
      valid_paths = 0
      @tmp_local_entries << tmpticket = @tmp_remote + ".ticket"
      File.open(tmpticket,'w') do |ticket|
        File.open(@config.ticket_file_include) do |file|
          file.each_line do |line|
            path = @config.remote_dir + "/" + line.chomp
            if remote_exists?(path)
              valid_paths += 1
              ticket.puts(line.chomp)
            else
              @logger.warn("! #{path} doesn't exist on server")
            end
          end
        end
      end
      # If none of the path in the ticket file exist on the server, chances are
      # something is wrong. Better stop here instead of doing something stupid.
      if valid_paths == 0
        raise Exception, "None of the path exist on the server. Check your configuration"
      end
      tmpticket
    end

    ###########################################################################
    # Part 2 : The diff/comparison
    ###########################################################################

    def checksum_files_before
      @logger.header("Checksuming all remote files")
      # We keep a MD5 of all the files to be able to check which files have been
      # added, modified and deleted after the comparison.
      Dir.glob("#{@tmp_remote}/**/*").each do |fullpath|
        unless File.directory?(fullpath)
          path = fullpath.sub("#{@tmp_remote}/","")
          @files_md5[path] = Digest::MD5.hexdigest(File.read(fullpath))
        end
      end
      @logger.info("> OK")
    end

    def compare_directories
      @logger.header("Launching visual diff")
      myexec(VISUAL_DIFF, @tmp_local, @tmp_remote)

      # Some tools return directly from the command line, even though the GUI is
      # still running. So we put the program in "waiting mode".
      confirm
    end

    def checksum_files_after
      @logger.header("Checking files for modifications")
      # The user has modified the destination files and everything should be
      # ready for deploy. We now check the files that have been added, modified,
      # and deleted.

      # First, added and modified
      Dir.glob("#{@tmp_remote}/**/*").each do |fullpath|
        unless File.directory?(fullpath)
          path = fullpath.sub("#{@tmp_remote}/","")
          md5_new =  Digest::MD5.hexdigest(File.read(fullpath))
          if md5_old = @files_md5.delete(path)
            unless md5_old == md5_new
              @files_modified << path
            end
          else
            @files_added << path
          end
        end
      end

      # Since we deleted all the keys for the files found, everything that is
      # left has been deleted.
      @files_deleted = @files_md5.keys

      # We show a log of everything
      @files_added.each { |path| @logger.info("> [A] #{@logger.green(path)}") }
      @files_deleted.each { |path| @logger.info("> [D] #{@logger.red(path)}") }
      @files_modified.each { |path| @logger.info("> [M] #{@logger.blue(path)}") }

      if @files_added.empty? && @files_deleted.empty? && @files_modified.empty?
        raise Exception, "No change detected. Nothing to deploy."
      end
    end

    def check_files_syntax
      @logger.header("Checking files for basic syntax errors")
      errors = 0
      checker = Checker.new
      @files_added.concat(@files_modified).each do |path|
        fullpath = "#{@tmp_remote}/#{path}"
        unless File.directory?(fullpath)
          unless checker.syntax_ok?(fullpath)
            @logger.warn("> ERROR : #{@logger.red(path)}")
            errors += 1
          end
        end
      end
      if errors == 0
        @logger.info("> OK")
      end
    end

    ###########################################################################
    # Part 3 : Deploying
    ###########################################################################

    def ask_for_confirmation
      confirm
    end

    def create_remote_backup
      if @config.local_backup || @config.remote_backup
        @logger.header("Creating remote backup")
        @tmp_local_entries << txt = @tmp_remote + ".backup.local.txt"
        @tmp_remote_entries << txt_remote = @tmp_remote + ".backup.remote.txt"

        # The backup will contain all the files deleted or modified.
        File.open(txt, 'w') do |file|
          @files_deleted.each {|entry| file.puts(entry)}
          @files_modified.each {|entry| file.puts(entry)}
        end

        if @config.remote_backup
          remote_backup_file = "#{@config.remote_backup_dir}/#{@config.remote_backup_filename}.remote.tgz"
        else
          remote_backup_file = "/tmp/#{@config.remote_backup_filename}.remote.tgz"
          @tmp_remote_entries << remote_backup_file
        end

        @sftp.upload!(txt, txt_remote)
        sshexec("cd \"#{@config.remote_dir}\" && tar -czf \"#{remote_backup_file}\" -T \"#{txt_remote}\"")

        if @config.remote_backup
          @logger.info("> Remote backup is located here: #{@logger.red(remote_backup_file)}")
        end

        if @config.local_backup
          local_backup_file = "#{@config.local_backup_dir}/#{@config.remote_backup_filename}.local.tgz"
          @sftp.download!(remote_backup_file, local_backup_file)
          @logger.info("> Local backup is located here: #{@logger.red(local_backup_file)}")
        end
      end
    end

    def deliver_payload
      @logger.header("Creating remote payload")
      @tmp_local_entries << txt = @tmp_remote + ".payload.local.txt"
      @tmp_remote_entries << txt_remote = @tmp_remote + ".payload.remote.txt"

      File.open(txt, 'w') do |file|
        @files_added.each {|entry| file.puts(entry)}
        @files_modified.each {|entry| file.puts(entry)}
      end

      if @config.local_backup
        local_deploy_file = "#{@config.local_backup_dir}/#{@config.remote_deploy_filename}.local.tgz"
      else
        @tmp_local_entries << local_deploy_file = "/tmp/#{@config.remote_deploy_filename}.local.tgz"
      end

      if @config.remote_backup
        remote_deploy_file = "#{@config.remote_backup_dir}/#{@config.remote_deploy_filename}.remote.tgz"
      else
        @tmp_remote_entries << remote_deploy_file = "/tmp/#{@config.remote_deploy_filename}.remote.tgz"
      end

      myexec("tar", "-czf", local_deploy_file, "-T", txt, {:chdir => @tmp_remote})
      @sftp.upload!(local_deploy_file, remote_deploy_file)
      @logger.info("> Remote payload is located here: #{@logger.red(remote_deploy_file)}") if @config.remote_backup
      @logger.info("> Local payload is located here: #{@logger.red(local_deploy_file)}") if @config.local_backup
      @logger.info("")

      create_remote_backup

      @logger.info(@logger.red("READY TO DEPLOY?"))
      @logger.info("THIS WILL UNTAR THE PAYLOAD ON THE SERVER")
      @logger.info("THIS WILL DELETE #{@files_deleted.size} FILES THE SERVER")
      confirm
      sshexec("cd \"#{@config.remote_dir}\" && tar -xzf \"#{remote_deploy_file}\"")

      @files_deleted.each do |path|
        begin
          fullpath = @config.remote_dir + '/' + path
          @logger.info("> DELETE: #{fullpath}")
          @sftp.remove!(fullpath)
        end
      end
    end

    ###########################################################################
    # System tools
    ###########################################################################

    def myexec(*cmd)
      chdir = Dir.getwd
      cwdir = Dir.getwd

      if cmd.instance_of? String
        @logger.command("$ " + cmd + " [local]")
      else
        @logger.command("$ " + cmd.join(" ") + " [local]")
        if cmd[-1].instance_of? Hash
          options = cmd.pop
          chdir = options[:chdir] if File.directory?(options[:chdir])
        end
      end

      Dir.chdir(chdir)
      system(*cmd)
      Dir.chdir(cwdir)

      if $?.exitstatus > 0
        raise Exception, "Command did not complete successfully"
      end
    end

    def sshexec(cmd)
      @logger.command("$ " + cmd + " [remote]")
      @ssh.exec!(cmd)
    end

    def confirm
      @logger.info("Press ENTER to continue, CTRL+C TO STOP")
      STDIN.gets
    end

    def remote_exists?(path)
      # This method is not perfect, but unfortunately net/sftp doesn't provide
      # a native way to do that. We could also use a combination of file?,
      # directory? and symlink?, but I don't know the cache strategy, so I don't
      # want this to result in 3 calls. So this should be good enough for what
      # we are doing.
      begin
        @sftp.stat!(path)
        return true
      rescue Net::SFTP::StatusException
        return false
      end
    end

    ###########################################################################
    # Visibility
    ###########################################################################

    public :cleanup
    public :check_remote_directories
    public :copy_local_ticket_files_to_tmp
    public :copy_remote_ticket_files_to_tmp
    public :checksum_files_before
    public :compare_directories
    public :checksum_files_after
    public :check_files_syntax
    public :ask_for_confirmation
    public :create_remote_backup
    public :deliver_payload

  end
end

# vim: set et ts=2 sw=2 sts=2:
