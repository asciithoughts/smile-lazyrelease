#
# Check files for syntax errors before deploy
#

module SComp
  class Checker

    # Very simple syntax error checking, based on the file extension.
    # The recognized types are: PHP (php), Ruby (rb).
    #
    # Returns:
    # - false if the file is empty
    # - true if the type is not supported
    # - true/false is the type is supported
    # - false in any other case (file doesn't exist, etc..)
    def syntax_ok?(path)
      result = false
      if path and not path.empty?
        if File.readable?(path) and File.size(path) > 0
          case path
          when /.*\.rb$/
            result = check_rb(path)
          when /.*\.php$/
            result = check_php(path)
          else
            result = true
          end
        end
      end
      result
    end

    private

    def check_rb(path)
      r = `ruby -c #{path} 2>&1`
      r =~ /^Syntax OK/
    end

    def check_php(path)
      r = `php -l #{path} 2>&1`
      r =~ /^No syntax errors detected/
    end

  end
end

# vim: set et ts=2 sw=2 sts=2:
