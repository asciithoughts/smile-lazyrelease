# Introduction

**LazyRelease** is a **merge & deploy** tool. It is for use in those rare but sad cases where you can't deploy using a VCS. It simplifies the process of manually deploying code, providing goodies such as automatic backup, archival of payloads, syntax checking, and the ability to deploy limited changes (i.e. you can pick and choose every line instead of deploying full
files).


## When NOT to use it

It you already have a good deploy strategy, preferably using a VCS.


## When to use it

There is basically only one use case: for some reason, you can't deploy from a VCS.


## What it does

**LazyRelease** process is simple:

* Get the files you want to modify **from** the server.
* **Merge** your changes using [Meld](http://meld.sourceforge.net/) (or [Araxis](http://www.araxis.com/) or whatever).
* Create a backup on the server of the files for easy rollback.
* Send the archive (`tar.gz`) and deploy it.


# Advantages

## Automatic backup

This is the **most important feature**: before deploying, LazyRelease makes a backup of all the files your payload will overwrite and/or delete. The backup file is kept on the server, and a local copy can be kept as well. This makes rollbacks easy: just **untar** the backup.


## Simple tools

LazyRelease simply uses `ssh` and `tar`. This allows anyone without access to the client to still go on the server, inspect the payloads, and rollback. It doesn't do any magic, it simply automates what you would be doing manually.

It does require **Ruby** on the client, but the server just needs `tar` and `ssh`.


## Merging

Instead of **deploying files**, you **merge changes**. This allows you to:

* Deploy patches instead of files.
* Verify what is actually on the server.
* Do manual corrections before deployment.

This allows to deal with the following cases:

* Several changes are in trunk, sometimes affecting the same files, but only one should be deployed. You can manually select each line or send full folders: it's all up to you.

* The VCS holds a certain configuration (production), and the destination server has a different configuration (development). Since you manually merge changes, you can adjust configuration variables at deploy time.

* The source code on the server differs from what is in the VCS. Obviously this should never happen, but sometime it does, and you may need to still deploy changes without breaking the server. Again, since you merge changes rather than deploying files, you can quickly spot differences, and either manually merge, or simply abort the deployment altogether. In any case, you never override a file without first looking at it.


## Compare

Since LazyRelease retrieves a set of files from the server and launches a visual diff on those files, it's also very practical to quickly check the differences between your code and what is on the server.


# Usage

    /home/john/projects/foo $ lazyrelease production 12345-bugfix.txt

Where:

* `production` is a server described in the configuration file `delivery.yml`.
* `12345-bugfix.txt` is a simple text file listing the files to deploy and/or compare.



## Configuration

The configuration file, `delivery.yml`, needs to be in the current directory. It's a simple [YAML](http://en.wikipedia.org/wiki/YAML) file (space is significant):

    development:
      host: example.com
      user: deploy
      port: 22
      password: password
      local_backup: true
      remote_backup: true
      remote_dir: /home/deploy/www
      local_backup_dir: /some/path
      remote_backup_dir: /home/deploy/backup

    staging:
      [...]

    production:
      [...]

Where:

* `development`, `staging`, `production`: name of this server configuration.
* `host`: hostname or IP of the server.
* `user` SSH user to connect with.
* `port`: SSH port to connect to. _optional, default is 22_.
* `password`: SSH password to use. It is much better to use SSH keys though. _optional_.
* `local_backup`: keep a local backup. _optional, default is false_.
* `local_backup_dir`: required if `local_backup` is true.
* `remote_backup`: keep a remote backup. _optional, default is false_.
* `remote_backup_dir`: required if `remote_backup` is true.


## List of files

You must also provide a list of path to compare, as a simple text file:

    libraries/foobar  # directory
    settings/foo.ini  # file
    index.php         # file
    - .svn            # path ignored
    - *.swp           # path ignored

NOTE: This file should be named `12345-description.txt` where `12345` is the ticket number in your bug tracker, and `description` is a brief description of it.


## Merge tool

The default merge tool is Meld. You can use a different one by setting the environment variable `MERGETOOL`:

    $ export MERGETOOL=araxis
    $ lazyrelease ...



# Example

`delivery.yml`:

    dev:
      host: server.dev
      user: deploy
      remote_dir: /home/deploy/www
      remote_backup: true
      remote_backup_dir: /home/deploy/backup

`12345-bugfix.txt`:

    bin
    lib
    - *.txt
    - .git

Execution:

    $ bin/lazyrelease dev 12345-bugfix.txt 
    Loading configuration
    - Hostname           : server.dev
    - Port               : 22
    - Username           : deploy
    - Remote dir         : /home/deploy/www
    - Remote backup dir  : /home/deploy/backup
    
    Opening SSH connection
    > OK
    
    Checking remote directories
    > OK
    
    Copying local ticket files to tmp
    $ mkdir /tmp/scomp-local-1323580264.991137 [local]
    $ tar -czf /tmp/scomp-local-1323580264.991137.local.tgz -T 12345-bugfix.include.txt -X 12345-bugfix.exclude.txt [local]
    $ tar -xzf /tmp/scomp-local-1323580264.991137.local.tgz chdir/tmp/scomp-local-1323580264.991137 [local]
    > OK
    
    Copying remote ticket files to tmp
    > Checking remote paths
    $ cd "/home/deploy/www" && tar -czf "/tmp/scomp-remote-1323580264.991137.remote.tgz" -T "/tmp/scomp-remote-1323580264.991137.ticket.remote" -X "/home/deploy/www/12345-bugfix.exclude.txt" [remote]
    $ mkdir /tmp/scomp-remote-1323580264.991137 [local]
    $ tar -xzf /tmp/scomp-remote-1323580264.991137.local.tgz chdir/tmp/scomp-remote-1323580264.991137 [local]
    > OK
    
    Checksuming all remote files
    > OK
    
    Launching visual diff
    $ araxis /tmp/scomp-local-1323580264.991137 /tmp/scomp-remote-1323580264.991137 [local]
    Press ENTER to continue, CTRL+C TO STOP

    
    Checking files for modifications
    > [A] bin/lazyrelease
    > [A] lib/scomp/checker.rb
    > [A] lib/scomp/config.rb
    > [A] lib/scomp/filemanager.rb
    > [A] lib/scomp/logger.rb
    > [A] lib/scomp/options.rb
    > [A] lib/scomp/runner.rb
    
    Checking files for basic syntax errors
    > OK
    
    Press ENTER to continue, CTRL+C TO STOP

    
    Creating remote backup
    $ cd "/home/deploy/www" && tar -czf "/home/deploy/backup/dev__2011-12-10__21h11m28s__backup__12345-bugfix.remote.tgz" -T "/tmp/scomp-remote-1323580264.991137.backup.remote.txt" [remote]
    > Remote backup is located here: /home/deploy/backup/dev__2011-12-10__21h11m28s__backup__12345-bugfix.remote.tgz
    
    Creating remote payload
    $ tar -czf /tmp/dev__2011-12-10__21h11m28s__payload__12345-bugfix.local.tgz -T /tmp/scomp-remote-1323580264.991137.payload.local.txt chdir/tmp/scomp-remote-1323580264.991137 [local]
    > Remote payload is located here: /home/deploy/backup/dev__2011-12-10__21h11m28s__payload__12345-bugfix.remote.tgz
    
    READY TO DEPLOY?
    THIS WILL UNTAR THE PAYLOAD ON THE SERVER
    THIS WILL DELETE 0 FILES THE SERVER
    Press ENTER to continue, CTRL+C TO STOP

    $ cd "/home/deploy/www" && tar -xzf "/home/deploy/backup/dev__2011-12-10__21h11m28s__payload__12345-bugfix.remote.tgz" [remote]
    
    Deleting local temporary files & directories
    > Deleting local  : 12345-bugfix.include.txt
    > Deleting local  : 12345-bugfix.exclude.txt
    > Deleting local  : /tmp/scomp-local-1323580264.991137
    > Deleting local  : /tmp/scomp-local-1323580264.991137.local.tgz
    > Deleting local  : /tmp/scomp-remote-1323580264.991137
    > Deleting local  : /tmp/scomp-remote-1323580264.991137.local.tgz
    > Deleting local  : /tmp/scomp-remote-1323580264.991137.ticket
    > Deleting local  : /tmp/scomp-remote-1323580264.991137.backup.local.txt
    > Deleting local  : /tmp/scomp-remote-1323580264.991137.payload.local.txt
    > Deleting local  : /tmp/dev__2011-12-10__21h11m28s__payload__12345-bugfix.local.tgz
    
    Deleting remote temporary files & directories
    > Deleting remote : /tmp/scomp-remote-1323580264.991137.remote.tgz
    > Deleting remote : /tmp/scomp-remote-1323580264.991137.ticket.remote
    > Deleting remote : /home/deploy/www/12345-bugfix.exclude.txt
    > Deleting remote : /tmp/scomp-remote-1323580264.991137.backup.remote.txt
    
    Closing SSH connection
    > OK
