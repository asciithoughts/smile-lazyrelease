#
# Specs for SComp::Checker
#

require 'tempfile'
require File.expand_path('../../lib/scomp/checker', __FILE__)

module SComp
  describe Checker do

    before do
      @checker = Checker.new
    end

    it "should raise an error with no argument" do
      expect { @checker.syntax_ok?() }.to raise_error
    end

    it "should return false with a bad argument" do
      @checker.syntax_ok?([]).should be_false
      @checker.syntax_ok?("").should be_false
    end

    it "should return false if the file does not exist" do
      @checker.syntax_ok?("/imaginary/path").should be_false
    end

    it "should return false if the file is empty" do
      Tempfile.new('spec') do |file|
        @checker.syntax_ok?(file.path).should be_false
      end
    end

    it "should return true if the type is not recognized" do
      Tempfile.new('spec') do |file|
        file.puts('foo')
        @checker.syntax_ok?(file.path).should be_true
      end
    end

    it "should return true if the ruby file is valid" do
      Tempfile.new(['spec', 'rb']) do |file|
        file.puts('foo = 1')
        @checker.syntax_ok?(file.path).should be_true
      end
    end

    it "should return false if the ruby file is not valid" do
      Tempfile.new(['spec', 'rb']) do |file|
        file.puts('if foo')
        @checker.syntax_ok?(file.path).should be_true
      end
    end

    it "should return true if the php file is valid" do
      Tempfile.new(['spec', 'php']) do |file|
        file.puts('<?php $foo = 1; ?>')
        @checker.syntax_ok?(file.path).should be_true
      end
    end

    it "should return false if the php file is not valid" do
      Tempfile.new(['spec', 'php']) do |file|
        file.puts('<?php foo = 1; ?>')
        @checker.syntax_ok?(file.path).should be_true
      end
    end

  end
end

# vim: set et ts=2 sw=2 sts=2:
