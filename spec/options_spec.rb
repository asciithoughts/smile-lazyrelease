#
# Specs for SComp::Options
#

require File.expand_path('../../lib/scomp/options', __FILE__)

module SComp
  describe Options do

    before do
      @out = StringIO.new
      @err = StringIO.new
    end

    it "should not accept 0 argument" do
      opts = Options.new([], @out, @err)
      opts.valid.should == false
    end

    it "should not accept 1 argument" do
      opts = Options.new(['foo'], @out, @err)
      opts.valid.should == false
    end

    it "should accept 2 arguments" do
      opts = Options.new(['foo', 'bar'], @out, @err)
      opts.valid.should == true
    end

    it "should not accept more than 2 arguments" do
      (3..10).each do |n|
        opts = Options.new(['foo'] * n, @out, @err)
        opts.valid.should == false
      end
    end

    it "should output 'usage' on error" do
      opts = Options.new([], @out, @err)
      @out.string.should =~ /Usage: scomp/
    end

    it "should recognize the debug option" do
      opts = Options.new(['-d', 'foo', 'bar'], @out, @err)
      opts.debug.should == true
    end

    it "should recognize the config option" do
      opts = Options.new(['-c', 'abcdef', 'foo', 'bar'], @out, @err)
      opts.config.should == 'abcdef'
    end

    it "should have 'delivery.yml' as a default config" do
      opts = Options.new(['foo', 'bar'], @out, @err)
      opts.config.should == "delivery.yml"
    end

    it "should have a server" do
      opts = Options.new(['foo', 'bar'], @out, @err)
      opts.server.should == "foo"
    end

    it "should have a ticket" do
      opts = Options.new(['foo', 'bar'], @out, @err)
      opts.ticket.should == "bar"
    end

  end
end

# vim: set et ts=2 sw=2 sts=2:
