#
# Specs for SComp::Logger
#

require File.expand_path('../../lib/scomp/logger', __FILE__)

module SComp
  describe MyLogger do

    before do
      @out = StringIO.new
      @logger = MyLogger.new(@out)
    end

    it "should have a default log level of INFO" do
      @logger.level.should == Logger::INFO
    end

    it "should output debug text" do
      @logger.level = Logger::DEBUG
      @logger.debug('abcdef')
      @out.string.should =~ /abcdef/
    end

    it "should output info text" do
      @logger.info('abcdef')
      @out.string.should =~ /abcdef/
    end

    it "should output warning text" do
      @logger.warn('abcdef')
      @out.string.should =~ /abcdef/
    end

    it "should output error text" do
      @logger.error('abcdef')
      @out.string.should =~ /abcdef/
    end

    it "should output fatal text" do
      @logger.fatal('abcdef')
      @out.string.should =~ /abcdef/
    end

    it "should allow a prefix" do
      @logger.prefix = '####'
      @logger.info('abcdef')
      @out.string.should =~ /####abcdef/
    end

    it "should output header text" do
      @logger.header('abcdef')
      @out.string.should =~ /abcdef/
    end

    it "should output command text" do
      @logger.command('abcdef')
      @out.string.should =~ /abcdef/
    end

    it "should know how to put text red" do
      @logger.red('abcdef').should =~ /abcdef/
      @out.string.should == ""
    end

    it "should know how to put text grey" do
      @logger.grey('abcdef').should =~ /abcdef/
      @out.string.should == ""
    end

    it "should know how to put text blue" do
      @logger.blue('abcdef').should =~ /abcdef/
      @out.string.should == ""
    end

    it "should know how to put text green" do
      @logger.green('abcdef').should =~ /abcdef/
      @out.string.should == ""
    end

    it "should know how to put text brown" do
      @logger.brown('abcdef').should =~ /abcdef/
      @out.string.should == ""
    end

  end
end

# vim: set et ts=2 sw=2 sts=2:
